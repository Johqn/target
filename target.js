const fs = require('fs')
const path = require('path')
const util = require('util')

const
    MOD_DELETE = 'delete',
    MOD_RENAME = 'rename'
    ;

const deleteTarget = (targetPath) => {
    fs.unlinkSync(targetPath)
}

const renameTarget = (targetPath) => {
    fs.renameSync(targetPath, getAvailableName(targetPath))
}

const getAvailableName = name => {
    const newName = name + '.old'
    return !fs.existsSync(newName) ?
        newName
        :
        getAvailableName(newName)
        ;
}

const mods = {
    delete: deleteTarget,
    rename: renameTarget
}

const target = (
    target,
    mod = MOD_RENAME
) => {

    const targetPath = path.resolve(target)

    if (fs.existsSync(targetPath)) {

        if (!mods[mod]) {
            throw new Error(`Unexpected mod : "${mod}".`)
        }
        mods[mod](targetPath)
    }

    return targetPath
}

module.exports = target 